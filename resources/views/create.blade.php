@extends('layouts.app')

@section('content')
<div class="container">
  <form action="{{ URL('article') }}" method="POST">
      @csrf
      <div class="form-group">
        <label >Image</label>
        <input name="image" type="text" class="form-control" aria-describedby="emailHelp">
      </div>
      <div class="form-group">
        <label>Title</label>
        <input name="title" type="text" class="form-control">
      </div>
      <div class="form-group">
        <label>Detail</label>
        <input name="detail" type="text" class="form-control">
      </div>
      @if ($errors->any())
      @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
        @endforeach   
      @endif
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
@endsection