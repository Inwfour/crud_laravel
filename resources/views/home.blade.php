@extends('layouts.app')

@section('content')
<div class="container">
    <div style="text-align: right">
        <a href="article/create" class="btn btn-success" style="margin: 10px; color: white">Create</a>
    </div>
    <div class="row">
        @foreach ($articles as $article)
        <div class="col-4">
            <div class="card" style="width: 18rem; margin-bottom: 10px">
              <img src="{{ $article->image }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">{{ $article->title }}</h5>
                    <p class="card-text">{{ $article->detail }}</p>
                </div>
                <div class="card-body">
                   
                    <form action="{{ URL('article/' . $article->id)}}" method="post">
                        @csrf
                        @method('delete')
                        <a href="{{ URL('article/' . $article->id . '/edit')}}" class="card-link btn btn-primary">Edit</a>
                        <button type="submit" class="card-link btn btn-danger">Delete</button>
                    </form>
                   
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection