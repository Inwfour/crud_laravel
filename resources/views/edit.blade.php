@extends('layouts.app')

@section('content')
<div class="container">
  <form action="{{ URL('article/' . $article->id) }}" method="POST">
      @csrf
      @method('PUT')
      <div class="form-group">
        <label >Image</label>
        <input value="{{ $article->image }}" name="image" type="text" class="form-control" aria-describedby="emailHelp">
      </div>
      <div class="form-group">
        <label>Title</label>
        <input value="{{ $article->title }}" name="title" type="text" class="form-control">
      </div>
      <div class="form-group">
        <label>Detail</label>
        <input value="{{ $article->detail }}" name="detail" type="text" class="form-control">
      </div>
      @if ($errors->any())
      @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
        @endforeach   
      @endif
      <button type="submit" class="btn btn-primary">Edit</button>
    </form>
  </div>
@endsection